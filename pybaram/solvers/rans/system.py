# -*- coding: utf-8 -*-
from pybaram.solvers.baseadvecdiff.system import BaseAdvecDiffSystem
from pybaram.solvers.rans import RANSElements, RANSIntInters, RANSBCInters, RANSMPIInters
from pybaram.backends.types import Queue

import numpy as np
import re


class RANSSystem(BaseAdvecDiffSystem):
    name = 'rans'
    _elements_cls = RANSElements
    _intinters_cls = RANSIntInters
    _bcinters_cls = RANSBCInters
    _mpiinters_cls = RANSMPIInters

    def __init__(self, be, cfg, msh, soln, comm, nreg, impl_op):
        # Save parallel infos
        self._comm = comm
        self.rank = rank = comm.rank

        # Load elements
        self.eles, elemap = self.load_elements(msh, soln, be, cfg, rank)
        self.ndims = next(iter(self.eles)).ndims

        # load interfaces
        self.iint = self.load_int_inters(msh, be, cfg, rank, elemap)

        # load bc
        self.bint, self.vint = self.load_bc_inters(msh, be, cfg, rank, elemap)

        # load mpiint
        self.mpiint = self.load_mpi_inters(msh, be, cfg, rank, elemap)

        # Load vertex
        self.vertex = vertex = self.load_vertex(msh, be, cfg, rank, elemap)

        # Load bnode
        bnode = self.load_bnode(msh, cfg, rank)

        # Construct kerenls
        self.eles.construct_kernels(vertex, bnode, nreg, impl_op)
        self.iint.construct_kernels(elemap, impl_op)
        self.bint.construct_kernels(elemap, impl_op)

        # Check reconstructed or not
        self._is_recon = (cfg.getint('solver', 'order', 1) > 1)

        if self.mpiint:
            # Construct MPI kernels
            self.mpiint.construct_kernels(elemap, impl_op)

        # Construct Vertex kernels
        self.vertex.construct_kernels(elemap)

        # Construct queue
        self._queue = Queue()

    def load_bnode(self, msh, cfg, rank):
        is_loaded = []

        if rank == 0:
            bnode = []
            for key in msh:
                m = re.match(r'bcon_([a-z_\d]+)_p([\d]+)$', key)

                if m:
                    # Collect boundary nodes
                    bname = m.group(1)
                    if bname not in is_loaded:
                        is_loaded.append(bname)
                        bcsect = 'soln-bcs-{}'.format(bname)
                        bctype = cfg.get(bcsect, 'type')

                        if bctype in ['adia-wall', 'isotherm-wall']:
                            bnode.append(msh['bnode_' + bname][:,:self.ndims])
            
            bnode = np.vstack(bnode)
        else:
            bnode = None

        bnode = self._comm.bcast(bnode, root=0)

        return bnode
